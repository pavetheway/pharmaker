<?php
namespace PharMaker;

use PharMaker\Util\Json;
use PharMaker\Util\ClassLoaderWrapper;
use PharMaker\Configuration\Target;
use PharMaker\Configuration\Configuration;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Phar;

class PharMaker
{
    private $config;
    private $rootDir;
    private $classFinder;
    private $signer;

    public function __construct(
        Configuration $config, $rootDir, ClassLoaderWrapper $classFinder, Signer $signer = null
    ) {
        $this->config = $config;
        $this->rootDir = $rootDir;
        $this->classFinder = $classFinder;
        $this->signer = Signer::new();
    }

    public function make(Target $target, OutputInterface $output)
    {
        $tmpPrefix = md5(uniqid() . microtime());
        $tmpName = $tmpPrefix . '.phar';
        $tmpFile = sys_get_temp_dir() . $tmpName;

        $included = [];

        $finder = Finder::create()->files()->in(
            $this->rootDir
        )->name('*.php')
            ->name('LICENSE')
            ->name('license')
            ->name('LICENSE.*')
            ->name('license.*')
            ->ignoreVCS(true)
            ->filter(function (\SplFileInfo $file) use ($included)
            {
                return true;
            });

        $filter = function (\SplFileInfo $file)
        {
            if (strlen($file) > 10) {
                return false;
            }
        };

        $phar = new Phar($tmpFile, null, $tmpName);

        //print_r(iterator_to_array($finder));
        //return;
        if (true)
        {
            $readFile = function($file)
            {
                switch (pathinfo($file, PATHINFO_EXTENSION)) {
                    case 'php': return php_strip_whitespace($file);
                    case 'json': return Json::encode(Json::read($file));
                    default: return file_get_contents($file);
                }
            };
        }
        else
        {
            $readFile = function($file)
            {
                return file_get_contents($file);
            };
        }

        $files = [];
        $rootLen = strlen($this->rootDir) + 1;
        $addFile = function($file) use ($phar, $rootLen, $output, $readFile, &$files)
        {
            $type = pathinfo($file, PATHINFO_EXTENSION);
            $pharFile = substr($file, $rootLen);
            //$output->write($file, true);
            //$output->write($file . "   " . $pharFile, true);
            $phar->addFromString($pharFile, $readFile($file));
        };

        $phar->startBuffering();

        foreach ($finder as $path => $value)
        {
            $addFile($path);
        }
        if ($target->getCompression())
        {
            $phar->compressFiles(Phar::GZ);
        }
        $phar->setStub($this->makeStub('index.php', $tmpName));

        $phar->stopBuffering();
        chmod($tmpFile, 0755);
        rename($tmpFile, $resultFile = $this->rootDir . '/' . $target->getOutFile());
        if (file_exists($sigFile = $resultFile . '.sig'))
        {
            unlink($sigFile);
        }
        if ($this->signer)
        {
            $this->signer->sign($resultFile);
        }
    }

    protected function makeStub($entryFile, $pharName, $zlib = false)
    {
        $stub = <<<EOT
#!/usr/bin/env php
<?php
define('PHARMAKER_IS_PHAR', true);
EOT;
        if ($this->signer)
        {
            $keys = json_encode($this->signer->getPublicKeys());
            $stub .= <<<EOT

define('PHARMAKER_PUBLIC_KEYS', '$keys');
EOT;
        }

        $stub .= <<<EOT

Phar::mapPhar('$pharName');
require 'phar://$pharName/$entryFile';
__HALT_COMPILER();
EOT;

        return $stub;
    }
}


use PharMaker\Internal;

class Signer
{
    private $keys;

    public function __construct(array $keys)
    {
        $this->keys = $keys;
    }

    public static function new()
    {
        $keys = [];

        {
            $ed25519 = sodium_crypto_sign_keypair();
            $ed25519Secret = sodium_crypto_sign_secretkey($ed25519);
            $ed25519Public = sodium_crypto_sign_publickey($ed25519);
            $keys['ed25519'] = [
                'secret'   => Internal\encode64($ed25519Secret),
                'public'   => Internal\encode64($ed25519Public),
            ];
        }
        {
            $rsa = openssl_pkey_new([
                "private_key_bits" => 3072,
                "private_key_type" => OPENSSL_KEYTYPE_RSA,
            ]);
            openssl_pkey_export($rsa, $rsaSecret);

            $details = openssl_pkey_get_details($rsa);
            $rsaPublic = $details['key'];
            openssl_free_key($rsa);

            $keys['rsa'] = [
                'secret'   => Internal\encode64($rsaSecret),
                'public'   => Internal\encode64($rsaPublic),
            ];
        }
        return new self($keys);
    }

    public function sign($file)
    {
        $contents = file_get_contents($file);
        $ed25519Signature = sodium_crypto_sign_detached(
            $contents,
            Internal\decode64($this->keys['ed25519']['secret'])
        );

        if (false == openssl_sign(
            $contents,
            $rsaSha512Signature,
            Internal\decode64($this->keys['rsa']['secret']),
            'sha512'
        )) {

        }

        if (false == openssl_sign(
            $contents,
            $rsaSha1Signature,
            Internal\decode64($this->keys['rsa']['secret']),
            'sha1'
        )) {

        }

        file_put_contents($file . '.sig', \PharMaker\Util\Json::encode([
            'ed25519' => Internal\encode64($ed25519Signature),
            'rsa-sha512' => Internal\encode64($rsaSha512Signature),
            'rsa-sha1' => Internal\encode64($rsaSha1Signature)
        ]));
    }

    public function getPublicKeys()
    {
        $out = [];
        foreach ($this->keys as $key => $value)
        {
            $out[$key] = $value['public'];
        }
        return $out;
    }
}