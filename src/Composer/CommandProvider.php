<?php
namespace PharMaker\Composer;

use Composer\Plugin\Capability\CommandProvider as CommandProviderCapability;

class CommandProvider implements CommandProviderCapability
{
    public function getCommands()
    {
        return [new MakeCommand, new KeyCommand];
    }
}
