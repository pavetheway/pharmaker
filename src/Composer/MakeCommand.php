<?php
namespace PharMaker\Composer;

use PharMaker\PharMaker;
use PharMaker\Util\ClassFinder;
use PharMaker\Util\ClassLoaderWrapper;
use PharMaker\Configuration\JsonFile;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Composer\Command\BaseCommand;
use Composer\Factory;

class MakeCommand extends BaseCommand
{
    protected function configure()
    {
        $this->setName('pharmake');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $composer = $this->getComposer();
        $composerConfig = $composer->getConfig();
        $vendorDir = $composerConfig->get('vendor-dir');

        $composerJson = Factory::getComposerFile();
        $rootDir = realpath(dirname($composerJson));

        $config = JsonFile::get();

        $loader = require $vendorDir . '/autoload.php';
        if (false == ($loader instanceOf ClassFinder))
        {
            $loader = ClassLoaderWrapper::wrapLoader($loader, $vendorDir, 'autoload.php', false);
        }
        //$loader->unregister();

        $loaderInitializer = self::findClassloaderInitializer();
        //$includeFiles = $loaderInitializer::$files;

        $includeFiles = require $vendorDir . '/composer/autoload_files.php';

        //$autolodedDiretctories = $this->getAutoloadedDirectories($loader);


        $autolodedDiretctories = [];


        $maker = new PharMaker($config, $rootDir, $loader);

        foreach ($config->getTargets() as $target)
        {
            $maker->make($target, $output);
        }
    }

    public static function findClassloaderInitializer()
    {
        $search = 'Composer\Autoload\ComposerStaticInit';
        $len = strlen($search);
        $found = [];
        foreach (get_declared_classes() as $class)
        {
            if (substr($class, 0, $len) == $search)
            {
                return $found[] = $class;
            }
        }
        if (count($found) > 0)
        {
            return array_pop($found);
        }
        throw new \Exception;
    }

    protected function getAutoloadedDirectories($loader)
    {
        $scanableDirs = [];

        $scanDirs = function(array $dirs) use (&$scanableDirs)
        {
            $scanableDirs = array_merge($scanableDirs, array_map('realpath', $dirs));
        };

        $scanNameSpaces = function(array $namespaces) use ($scanDirs)
        {
            foreach ($namespaces as $ns => $dirs) $scanDirs($dirs);
        };

        $scanNameSpaces($loader->getPrefixesPsr4());
        $scanNameSpaces($loader->getPrefixes());
        $scanDirs($loader->getFallbackDirsPsr4());
        $scanDirs($loader->getFallbackDirs());

        return $scanableDirs;//Finder::create()->files()->in($scanableDirs)->name('*.php');
    }
}
