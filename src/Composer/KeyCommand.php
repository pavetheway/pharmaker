<?php
namespace PharMaker\Composer;

use PharMaker\PharMaker;
use PharMaker\Util\ClassFinder;
use PharMaker\Util\ClassLoaderWrapper;
use PharMaker\Configuration\JsonFile;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Composer\Command\BaseCommand;
use Composer\Factory;

class KeyCommand extends BaseCommand
{
    protected function configure()
    {
        $this->setName('pharmake-keys');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = $this->getIO();
        $keys = $io->select(
            "Which key types to use?",
            ["ed25519", "rsa"],
            "1,0",
            false,
            'Value "%s" is invalid',
            true
        );
        $io->write(var_export($keys, true));
    }
}
