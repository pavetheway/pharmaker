<?php
namespace PharMaker\Composer;

use PharMaker\Configuration\JsonFile;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\Plugin\PluginInterface;
use Composer\Plugin\Capable;
use Composer\IO\IOInterface;
use Composer\Composer;

class Plugin implements PluginInterface, Capable, EventSubscriberInterface
{
    public function activate(Composer $composer, IOInterface $io) { }

    public function getCapabilities()
    {
        return ['Composer\Plugin\Capability\CommandProvider' => 'PharMaker\Composer\CommandProvider'];
    }

    public static function getSubscribedEvents()
    {
        return [
            'post-autoload-dump' => 'onDump',
        ];
    }

    public function onDump($event)
    {
        $composer = $event->getComposer();
        $composerConfig = $composer->getConfig();
        $vendor = $composerConfig->get('vendor-dir');

        if (file_exists($jsonFile = $vendor . '/pharmaker_classinfo.json'))
        {
            unlink($jsonFile);
        }
        if (file_exists($origLoader = $vendor . '/autoload_orig.php'))
        {
            unlink($origLoader);
        }

        foreach (JsonFile::get()->getTargets() as $target)
        {
            if ($target->getMergeFrequentFiles())
            {
                $this->activateClassLoaderWrapper($vendor);
                return;
            }
        }
    }

    protected function activateClassLoaderWrapper($dir)
    {
    $template = <<<EOT
<?php

if (false == function_exists("PharMakerLoaderInit{RAND}"))
{
    function PharMakerLoaderInit{RAND}()
    {
        static \$loader;
        if (null == \$loader)
        {
            \$loader = require __DIR__ . "/autoload_orig.php";
            if (false == (defined('PHARMAKER_IS_PHAR') && PHARMAKER_IS_PHAR))
            {
                \$loader = \\PharMaker\\Util\\ClassLoaderWrapper::wrapLoader(\$loader, __DIR__, true);
            }
        }
        return \$loader;
    }
}
return PharMakerLoaderInit{RAND}();
EOT;

        $rand = md5(mt_rand());
        rename($newFile = $dir . '/autoload.php', $dir . '/autoload_orig.php');
        file_put_contents($newFile, str_replace(["{RAND}"], [$rand], $template));
    }
}
