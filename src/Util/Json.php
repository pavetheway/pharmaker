<?php
namespace PharMaker\Util;

use Seld\JsonLint\JsonParser;

class Json
{
    public static function read($file, $usefulExceptions = true)
    {
        return self::decode(file_get_contents($file));
    }

    public static function decode($json, $usefulExceptions = true)
    {
        if (($usefulExceptions && class_exists('Seld\JsonLint\JsonParser')) || false == extension_loaded('json'))
        {
            $parser = new JsonParser;
            return $parser->parse($json, JsonParser::PARSE_TO_ASSOC);
        }
        return json_decode($json, true);
    }

    public static function write($file, $data, $prettyPrint = true)
    {
        return is_integer(file_put_contents($file, self::encode($data, $prettyPrint)));
    }

    public static function encode($data, $prettyPrint = true)
    {
        $options = JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP;

        if ($prettyPrint)
        {
            $options |= JSON_PRETTY_PRINT;
        }

        return json_encode($data, $options);
    }
}
