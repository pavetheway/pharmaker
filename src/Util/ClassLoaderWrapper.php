<?php
namespace PharMaker\Util;

class ClassLoaderWrapper
{
    private $file;

    private $wrapped;

    private $data = [];

    public function __construct($wrapped, $dir)
    {
        $this->wrapped = $wrapped;

        if (file_exists($this->file = $dir . '/pharmaker_classinfo.json'))
        {
            $this->data = json_decode(file_get_contents($this->file), true);
        }
    }

    public function __destruct()
    {
        file_put_contents($this->file, json_encode(
            $this->data,
            JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_PRETTY_PRINT
        ));
    }

    public static function wrapLoader($wrapped, $dir, $register = true)
    {
        $class = get_called_class();
        $loader = new $class($wrapped, $dir);

        if ($register)
        {
            $loader->register();
        }

        return $loader;
    }

    public function register()
    {
        foreach (spl_autoload_functions() as $func)
        {
            if (
                is_array($func) &&  is_object($func[0])
                && spl_object_hash($func[0]) == spl_object_hash($this->wrapped)
            ) {
                spl_autoload_unregister([$func[0], $func[1]]);
            }
        }
        spl_autoload_register([$this, 'loadClass'], true, true);
    }

    public function unregister()
    {
        spl_autoload_unregister(array($this, 'loadClass'));
    }

    public function getClasses()
    {
        $out = [];
        foreach ($this->data as $class => $info)
        {
            $out[] = new ClassInfo(
                $class, $info['type'], $info['hits'], $info['parent'],
                $info['implements'], $info['uses']
            );
        }
        return $out;
    }

    public function findFile($class)
    {
        if (is_string($file = $this->wrapped->findFile($class)))
        {
            return $file;
        }
    }

    public function loadClass($class)
    {
        if ($file = $this->findFile($class))
        {
            require $file;

            if (!isset($this->data[$class]))
            {
                $this->data[$class] = [
                    'hits' => 1,
                    'updated'   => 0
                ];
            }
            else
            {
                $this->data[$class]['hits']++;
            }

            if ($this->data[$class]['updated'] < ($new = filemtime($file)))
            {
                $deps= [];
                $ref = new \ReflectionClass($class);

                if ($parent = $ref->getParentClass())
                {
                    $parent = $parent->name;
                }
                else
                {
                    $parent = null;
                }

                $type = 'class';
                if ($ref->isInterface())
                {
                    $type = 'interface';
                }
                elseif ($ref->isTrait())
                {
                    $type = 'trait';
                }

                $this->data[$class]['updated'] = $new;
                $this->data[$class]['type'] = $type;
                $this->data[$class]['parent'] = $parent;
                $this->data[$class]['implements'] = $ref->getInterfaceNames();
                $this->data[$class]['uses'] = $ref->getTraitNames();
            }

            return true;
        }
    }
}

class ClassInfo
{
    public function __construct($name, $type, $hits, $parent, array $implements, array $uses)
    {
        $this->name = $name;
        $this->type = $type;
        $this->hits = $hits;
        $this->parent = $parent;
        $this->implements = $implements;
        $this->uses = $uses;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getParentClass()
    {
        return $this->parent;
    }

    public function isInterface()
    {
        return $this->type == 'interface';
    }

    public function isUserDefined()
    {
    }
}
