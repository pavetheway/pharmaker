<?php
namespace PharMaker\Configuration;

interface Configuration
{
    const DEFAULTS = [
        "name"  => "myapp",
        "output_directory" => "pharmake.out",
        "targets"   => [
            "minimal" => [
                "compression"  => true,
                "merge_frequent_files"  => true,
                "exclude" => [

                ],
            ],
            "compatible" => [
                "compression"  => true,
                "merge_frequent_files"  => true,
                "exclude" => [

                ],
            ],
            "extra-compatible" => [
                "compression"  => false,
                "merge_frequent_files"  => false,
                "exclude" => [

                ],
            ]
        ],
        "globals" => [
            "exclude" => [
                "/\/[T|t]ests?\//",
                "/\/pharmaker\//"
            ]
        ]
    ];
}