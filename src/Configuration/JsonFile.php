<?php
namespace PharMaker\Configuration;

use PharMaker\Util\Json;
use Composer\Factory;

class JsonFile extends ConfigClass implements Configuration
{
    private $data;

    public function __construct($file)
    {
        $this->config = Json::read($file);
        $this->file = $file;

        $that = $this;
        $accessor = function($key)
        {
            return $this->config[$key];
        };

        foreach ($this->config['targets'] as $name => $config)
        {
            $this->config['targets'][$name] = new Target($name, $config, $accessor);
        }
    }

    public function getName()
    {
        return $this->config['name'];
    }

    public function getGlobalExcludes()
    {
        return $this->config['globals']['exclude'];
    }

    public function getOuputDir()
    {
        return $this->config['output_directory'];
    }

    public function getTargets()
    {
        return $this->config['targets'];
    }

    public function getStubfile()
    {
        return isset($this->config['stubfile']) ? $this->config['stubfile'] : null;
    }

    public function save()
    {
        Json::write($this->file, $this->config);
    }

    public static function get()
    {
        if (class_exists(Factory::CLASS))
        {
            $composerJson = Factory::getComposerFile();
            $root = realpath(dirname($composerJson));
        } else {
            $root = realpath($_SERVER['argv'][0]);
        }

        if (false == file_exists($file = ($root . '/pharmaker.json')))
        {
            Json::write($file, self::DEFAULTS);
        }

        return new self($file);
    }
}
