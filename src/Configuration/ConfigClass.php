<?php
namespace PharMaker\Configuration;

use Doctrine\Common\Inflector\Inflector;

class ConfigClass
{
    public function __debugInfo()
    {
        $out = [];
        foreach (get_class_methods($this) as $method)
        {
            if (strlen($method) !== 3 && substr($method, 0, 3) == 'get')
            {
                $name = Inflector::camelize(substr($method, 3));
                $out[$name] = call_user_func([$this, $method]);
            }
        }
        return $out;
    }
}