<?php
namespace PharMaker\Configuration;

use PharMaker\Util\Json;
use JsonSerializable;
use Closure;

class Target extends ConfigClass implements JsonSerializable
{
    private $name;

    private $config;

    private $parentAccessor;

    public function __construct($name, array $config, Closure $parentAccessor)
    {
        $this->name = $name;
        $this->config = $config;
        $this->parentAccessor = $parentAccessor;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getOutFile()
    {
        return call_user_func($this->parentAccessor, 'output_directory') . "/" . $this->name;
    }

    public function getCompression()
    {
        return $this->config['compression'];
    }

    public function getExcludes($includeGlobals = false)
    {
        if ($includeGlobals)
        {
            $globals = call_user_func($this->parentAccessor, 'globals');
            return array_merge($this->config['exclude'], $globals['exclude']);
        }
        return $this->config['exclude'];
    }

    public function getMergeFrequentFiles()
    {
        return $this->config['merge_frequent_files'];
    }

    public function jsonSerialize()
    {
        return $this->config;
    }
}
