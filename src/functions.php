<?php
/**
 * This file will be embedded to the phar archive.
 */
namespace PharMaker
{
    function isPhar()
    {
        return defined('PHARMAKER_IS_PHAR') && PHARMAKER_IS_PHAR;
    }

    use PharMaker\Internal;
    use PharMaker\Internal\Sodium;

    function verifyBin($bin, $signatures, array $keys = [])
    {
        $keys = Internal\getKeys($keys);

        if (isset($keys['ed25519']))
        {

        }
        if (isset($keys['rsa']))
        {
            
        }

        if ($ed25519PublicKey && Sodium\hasSodium())
        {
            if (Sodium\verify($signature, $message, $ed25519PublicKey))
            {

            }
        }

        if ($rsaPublicKey && function_exists('openssl_verify'))
        {
            
        }
    }
}

namespace PharMaker\Internal
{
    function getKeys(array $keys)
    {
        if (count($keys) == 0)
        {
            if (false == defined('PHARMAKER_PUBLIC_KEYS'))
            {

            }
            $keys = json_decode(PHARMAKER_PUBLIC_KEYS);
        }
        $out = [];
        foreach ($keys as $key => $value)
        {
            $out[$key] = decode64($value);
        }
        return $out;
    }

    function encode64($data)
    {
        return str_replace(
            ['+','/'],
            ['-','_'],
            base64_encode($data)
        );
    }

    function decode64($data)
    {
        return base64_decode(str_replace(
            ['-','_'],
            ['+','/'],
            $data
        ));
    }
}

namespace PharMaker\Internal\Sodium
{
    function hasSodium()
    {
        return function_exists('sodium_crypto_sign_verify_detached')
            || function_exists('Sodium\\crypto_sign_verify_detached');
    }

    function verify($signature, $message, $publicKey)
    {
        if (function_exists('sodium_crypto_sign_verify_detached'))
        {
            return \sodium_crypto_sign_verify_detached($signature, $message, $publicKey);
        }
        return \Sodium\crypto_sign_verify_detached($signature, $message, $publicKey);
    }
}

namespace
{
    /**
     * PHP 7.2+ polyfill for posix_isatty function that
     * is often used in console applications.
     */
    if (
        false == function_exists('posix_isatty')
        && PHP_VERSION_ID >= 70200
    ) {
        function posix_isatty($recource)
        {
            return stream_isatty($recource);
        }
    }
}
